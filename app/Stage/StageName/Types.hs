module Stage.StageName.Types where

import RIO

import Engine.Stage.Component qualified as Stage
import Render.DescSets.Set0 qualified as Set0
import Vulkan.Core10 qualified as Vk

import Global.Render qualified as Render

type Stage = Render.Stage FrameResources RunState
type Frame = Render.Frame FrameResources
type Scene = Render.StageScene RunState FrameResources
type RecordCommands a = Vk.CommandBuffer -> FrameResources -> Word32 -> Render.StageFrameRIO FrameResources RunState a

type Resources = Stage.Resources Render.RenderPasses Render.Pipelines RunState FrameResources

type UpdateBuffers a = RunState -> FrameResources -> Render.StageFrameRIO FrameResources RunState a

-- Double-buffered resources used in rendering
data FrameResources = FrameResources
  { frScene :: Set0.FrameResource '[Set0.Scene]
  }

-- Inter-frame persistent resources
data RunState = RunState
  { rsSceneP :: Set0.Process
  }
