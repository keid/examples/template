module Stage.StageName.Resources where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Engine.Camera qualified as Camera
import Engine.Stage.Component qualified as Stage
import Engine.Types (StageRIO, StageSetupRIO)
import Engine.Vulkan.Types (Queues)
import Engine.Worker qualified as Worker
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Scene
import Resource.Region (ReleaseKey)
import Resource.Region qualified as Region
import Vulkan.Core10 qualified as Vk

import Global.Render qualified
import Stage.StageName.Types qualified as StageName

component :: StageName.Resources
component = Stage.Resources
  { rInitialRS = initialRunState
  , rInitialRR = initialRecycledResources
  }

initialRunState :: StageSetupRIO (ReleaseKey, StageName.RunState)
initialRunState = Region.run do
  ortho <- Region.local $
    Worker.registered Camera.spawnOrthoPixelsCentered

  rsSceneP <- Region.local $
    Worker.registered (Worker.spawnMerge1 mkScene ortho)

  pure StageName.RunState{..}
  where
    mkScene p = Scene.emptyScene
      { Scene.sceneProjection = p.projectionTransform
      }

initialRecycledResources
  :: Queues Vk.CommandPool
  -> Global.Render.RenderPasses
  -> Global.Render.Pipelines
  -> ResourceT (StageRIO StageName.RunState) StageName.FrameResources
initialRecycledResources _pools _renderpasses pipelines = do
  frScene <- Scene.allocate
    (Basic.getSceneLayout pipelines)
    Nothing
    Nothing
    Nothing
    mempty
    Nothing

  pure StageName.FrameResources{..}
