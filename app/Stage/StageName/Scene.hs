module Stage.StageName.Scene where

import RIO

import Engine.Stage.Component qualified as Stage
import Engine.Types qualified as Engine
import Engine.Vulkan.Swapchain qualified as Swapchain
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Scene
import Render.Pass (usePass)

import Stage.StageName.Types qualified as StageName

component :: StageName.Scene
component = Stage.Scene
  { scBeforeLoop = pure ()
  , scUpdateBuffers = updateBuffers
  , scRecordCommands = recordCommands
  }

updateBuffers :: StageName.UpdateBuffers ()
updateBuffers StageName.RunState{..} StageName.FrameResources{..} = do
  Scene.observe rsSceneP frScene

recordCommands :: StageName.RecordCommands ()
recordCommands cb StageName.FrameResources{} imageIndex = do
  frame <- asks snd
  usePass frame.fRenderpass.rpForwardMsaa imageIndex cb do
    Swapchain.setDynamicFullscreen cb frame.fSwapchainResources
