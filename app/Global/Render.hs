module Global.Render where

import Engine.Stage.Component qualified as Stage
import Engine.Types qualified as Engine
import Render.Basic qualified as Basic

type Frame = Engine.Frame RenderPasses Pipelines
type Stage = Engine.Stage RenderPasses Pipelines
type StageFrameRIO fr rs = Engine.StageFrameRIO RenderPasses Pipelines fr rs
type StageResources fr rs = Stage.Resources RenderPasses Pipelines fr rs
type StageScene fr rs = Stage.Scene RenderPasses Pipelines fr rs

type RenderPasses = Basic.RenderPasses

type Pipelines = Basic.Pipelines

component :: Basic.Rendering st
component = Basic.rendering_
